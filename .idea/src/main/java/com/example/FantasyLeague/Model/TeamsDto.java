package com.example.FantasyLeague.Model;

import com.example.FantasyLeague.Entity.TeamPlayers;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.List;

@ApiModel(description = "Dto for the teams entity for creating teams with list of players")
public class TeamsDto {

    @ApiModelProperty(name = "teamName",example = "csk",required = true)
    private String teamName;
    @ApiModelProperty(name = "teamplayers for the teams")
    private List<TeamPlayers> players = new ArrayList<>(15);

    public TeamsDto() {

    }

    public TeamsDto(String teamName, List<TeamPlayers> players) {
        this.teamName = teamName;
        this.players = players;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public List<TeamPlayers> getPlayers() {
        return players;
    }

    public void setPlayers(List<TeamPlayers> players) {
        this.players = players;
    }

    @Override
    public String toString() {
        return "TeamsDto{" +
                "teamName='" + teamName + '\'' +
                ", players=" + players +
                '}';
    }
}