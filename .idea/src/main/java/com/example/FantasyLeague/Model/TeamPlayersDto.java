package com.example.FantasyLeague.Model;

import com.example.FantasyLeague.Entity.TeamPlayers;




public class TeamPlayersDto {

    private String playerName;

    private int Age;
    private TeamPlayers.Role role;

    private String Nationality;
    private TeamsDto teamsDto;
    private FantasyTeamDto fantasyTeamDto;

    public TeamPlayersDto() {
    }

    public TeamPlayersDto(String playerName, int age, TeamPlayers.Role role, String nationality, TeamsDto teamsDto, FantasyTeamDto fantasyTeamDto) {
        this.playerName = playerName;
        Age = age;
        this.role = role;
        Nationality = nationality;
        this.teamsDto = teamsDto;
        this.fantasyTeamDto = fantasyTeamDto;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public TeamPlayers.Role getRole() {
        return role;
    }

    public void setRole(TeamPlayers.Role role) {
        this.role = role;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public TeamsDto getTeamsDto() {
        return teamsDto;
    }

    public void setTeamsDto(TeamsDto teamsDto) {
        this.teamsDto = teamsDto;
    }

    public FantasyTeamDto getFantasyTeamDto() {
        return fantasyTeamDto;
    }

    public void setFantasyTeamDto(FantasyTeamDto fantasyTeamDto) {
        this.fantasyTeamDto = fantasyTeamDto;
    }

    @Override
    public String toString() {
        return "TeamPlayersDto{" +
                "playerName='" + playerName + '\'' +
                ", Age=" + Age +
                ", role=" + role +
                ", Nationality='" + Nationality + '\'' +
                ", teamsDto=" + teamsDto +
                ", fantasyTeamDto=" + fantasyTeamDto +
                '}';
    }
}

