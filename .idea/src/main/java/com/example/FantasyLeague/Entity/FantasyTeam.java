package com.example.FantasyLeague.Entity;



import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table

@ApiModel(description = "for creating fantasy team with the given teams")
public class FantasyTeam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Fantasy_Team_Id;
    @ApiModelProperty(name = "fantasyteamname",example = "csk vs rcb",required = true)
    private String Fantasy_team_name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "fantasy_team_id")
    @ApiModelProperty(name = "teamplayers for creating fantasy team")
    List<TeamPlayers> teamPlayers=new ArrayList<>(11);

    public FantasyTeam() {
    }

    public FantasyTeam(Long fantasy_Team_Id, String fantasy_team_name, List<TeamPlayers> teamPlayers) {
        Fantasy_Team_Id = fantasy_Team_Id;
        Fantasy_team_name = fantasy_team_name;
        this.teamPlayers = teamPlayers;
    }

    public Long getFantasy_Team_Id() {
        return Fantasy_Team_Id;
    }

    public void setFantasy_Team_Id(Long fantasy_Team_Id) {
        Fantasy_Team_Id = fantasy_Team_Id;
    }

    public String getFantasy_team_name() {
        return Fantasy_team_name;
    }

    public void setFantasy_team_name(String fantasy_team_name) {
        Fantasy_team_name = fantasy_team_name;
    }

    public List<TeamPlayers> getTeamPlayers() {
        return teamPlayers;
    }

    public void setTeamPlayers(List<TeamPlayers> teamPlayers) {
        this.teamPlayers = teamPlayers;
    }

    @Override
    public String toString() {
        return "FantasyTeam{" +
                "Fantasy_Team_Id=" + Fantasy_Team_Id +
                ", Fantasy_team_name='" + Fantasy_team_name + '\'' +
                ", teamPlayers=" + teamPlayers +
                '}';
    }
}
