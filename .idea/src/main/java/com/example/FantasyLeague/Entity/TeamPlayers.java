package com.example.FantasyLeague.Entity;



import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;


@Entity
@Table(name = "Players")
@ApiModel(description = "creating players for each team for creating fantasy team")
public class TeamPlayers {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)


    private Long id;
    @ApiModelProperty(name = "playername",example = "yuvi",required = true)
    private String playerName;
    @Min(17)@Max(60)
    @ApiModelProperty(name = "Age",example = "25",required = true)
    private int Age;
    @ApiModelProperty(name = "Role",example = "BATSMAN",required = true)
    private Role role;
    @ApiModelProperty(name = "nationality",example = "india",required = true)
    private String Nationality;
    @ManyToOne(cascade = CascadeType.ALL)

    private Teams teams;
    @ManyToOne(cascade = CascadeType.ALL)
    private FantasyTeam fantasyTeam;


    public enum Role{
        BATSMAN,
        WICKETKEEPER,
        ALLROUNDER,
        BOWLER,
    }

    public TeamPlayers() {
    }

    public TeamPlayers(Long id, String playerName, int age, Role role, String nationality, Teams teams, FantasyTeam fantasyTeam) {
        this.id = id;
        this.playerName = playerName;
        Age = age;
        this.role = role;
        Nationality = nationality;
        this.teams = teams;
        this.fantasyTeam = fantasyTeam;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public Teams getTeams() {
        return teams;
    }

    public void setTeams(Teams teams) {
        this.teams = teams;
    }

    public FantasyTeam getFantasyTeam() {
        return fantasyTeam;
    }

    public void setFantasyTeam(FantasyTeam fantasyTeam) {
        this.fantasyTeam = fantasyTeam;
    }

    @Override
    public String toString() {
        return "TeamPlayers{" +
                "id=" + id +
                ", playerName='" + playerName + '\'' +
                ", Age=" + Age +
                ", role=" + role +
                ", Nationality='" + Nationality + '\'' +
                ", teams=" + teams +
                ", fantasyTeam=" + fantasyTeam +
                '}';
    }
}

