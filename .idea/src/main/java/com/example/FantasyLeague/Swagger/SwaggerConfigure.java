package com.example.FantasyLeague.Swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfigure {
    @Bean
    public Docket swaggerConfig(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.FantasyLeague"))
                .build()
                .apiInfo(new ApiInfo("Fantasy team APP","Implemented simple fantasy app where you can create fantasy team",
                        "1.0","https://gitlab.com/sharanbalaji18/fantasy-team/-/issues/2","Sharan balaji","",""));

    }
}
