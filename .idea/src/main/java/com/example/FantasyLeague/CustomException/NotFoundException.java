package com.example.FantasyLeague.CustomException;




public class NotFoundException extends RuntimeException{
    private String Message;
    private String path;

    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }

    public NotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public NotFoundException(String message, String path) {
        Message = message;
        this.path = path;
    }

    public NotFoundException(String message, String message1, String path) {
        super(message);
        Message = message1;
        this.path = path;
    }

    public NotFoundException(String message, Throwable cause, String message1, String path) {
        super(message, cause);
        Message = message1;
        this.path = path;
    }

    public NotFoundException(Throwable cause, String message, String path) {
        super(cause);
        Message = message;
        this.path = path;
    }

    public NotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String message1, String path) {
        super(message, cause, enableSuppression, writableStackTrace);
        Message = message1;
        this.path = path;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "NotFoundException{" +
                "Message='" + Message + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
