package com.example.FantasyLeague.Repository;

import com.example.FantasyLeague.Entity.Teams;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamsRepository extends JpaRepository<Teams,Long> {

}
