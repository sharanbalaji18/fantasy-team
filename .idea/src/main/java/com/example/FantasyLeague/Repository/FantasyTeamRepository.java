package com.example.FantasyLeague.Repository;

import com.example.FantasyLeague.Entity.FantasyTeam;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FantasyTeamRepository extends JpaRepository<FantasyTeam,Long> {

}
