package com.example.FantasyLeague.Service;

import com.example.FantasyLeague.CustomException.NotFoundException;
import com.example.FantasyLeague.Entity.TeamPlayers;
import com.example.FantasyLeague.Entity.Teams;
import com.example.FantasyLeague.Model.TeamPlayersDto;
import com.example.FantasyLeague.Model.TeamsDto;
import com.example.FantasyLeague.Repository.TeamPlayerRepository;
import com.example.FantasyLeague.Repository.TeamsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeamsService {
    @Autowired
    private TeamsRepository teamsRepository;
    @Autowired
    private TeamPlayerRepository teamPlayerRepository;

    Logger log = LoggerFactory.getLogger(TeamsService.class);

    public List<TeamsDto> getAllTeams() throws NotFoundException {
        List<TeamsDto> teamPlayersDtoList = teamsRepository.findAll().stream().map(teams -> convertEntityToDto(teams)).collect(Collectors.toList());
        log.debug("Checking whether the list is empty");
        if (teamPlayersDtoList.isEmpty()) {
            log.error("Exception in getAllTeams method");
            throw new NotFoundException("teams not found ", "/teams");
        }
        log.info("returning the list");
        return teamPlayersDtoList;
    }

    public TeamsDto convertEntityToDto(Teams teams) {
        TeamsDto teamPlayersDto = new TeamsDto();

        teamPlayersDto.setTeamName(teams.getTeamName());
        teamPlayersDto.setPlayers(teams.getPlayers());


        return teamPlayersDto;
    }

    public Teams convertEntityToTeams(Teams teams, TeamsDto teamPlayersDto) {
        TeamPlayersDto teamPlayersDto1 = new TeamPlayersDto();
        teams.setTeamName(teamPlayersDto.getTeamName());

        teams.setPlayers(teamPlayersDto.getPlayers());
        return teams;

    }


    public TeamsDto addTeams(TeamsDto teamPlayersDto) {
        Teams teams = new Teams();
        TeamPlayers teamPlayers = new TeamPlayers();
        teamsRepository.save(convertEntityToTeams(teams, teamPlayersDto));
        return teamPlayersDto;

    }

    public HttpStatus updateTeams(TeamsDto teamPlayersDto, long id) {
        Teams teamUpdate = teamsRepository.findById(id).orElseThrow(() -> new NotFoundException("the team which you want to update is not found", "/teams/{id}"));
        teamsRepository.save(convertEntityToTeams(teamUpdate, teamPlayersDto));
        return HttpStatus.ACCEPTED;
    }

    public HttpStatus deleteTeams(long id) {
        Teams teamDelete = teamsRepository.findById(id).orElseThrow(() -> new NotFoundException("the team which you want to delete is not found", "/teams/{id}"));

        teamsRepository.delete(teamDelete);

        return  HttpStatus.OK;

    }

    public ResponseEntity<List<TeamsDto>> getAllTeamsWithSorting(String field) {
        List<TeamsDto> teamsDtos = teamsRepository.findAll(Sort.by(Sort.Direction.ASC, field)).stream().map(teams -> convertEntityToDto(teams)).collect(Collectors.toList());
        log.debug("Checking whether the list is empty");
        if (teamsDtos.isEmpty()) {
            log.error("Exception in getAllTeams method");
            throw new NotFoundException("teams not found ", "/teams");
        }
        log.info("returning the list");
        return new ResponseEntity<>(teamsDtos, HttpStatus.OK);
    }
}
