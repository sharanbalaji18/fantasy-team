package com.example.FantasyLeague.Entity;



import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity

@Table(name = "Teams")
@ApiModel(description = "this is the class for creating teams")
public class Teams {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "team_name",unique = true)
    @ApiModelProperty(name = "teamName",example = "csk",required = true)
    private String teamName;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="team_id",referencedColumnName = "id")

    @ApiModelProperty(name = "teamPlayers for creating teams")
    List<TeamPlayers> players=new ArrayList<>(15);


    public Teams() {
    }

    public Teams(Long id, String teamName, List<TeamPlayers> players) {
        this.id = id;
        this.teamName = teamName;
        this.players = players;


    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public List<TeamPlayers> getPlayers() {
        return players;
    }

    public void setPlayers(List<TeamPlayers> players) {
        this.players= players;
    }

    @Override
    public String toString() {
        return "Teams{" +
                "id=" + id +
                ", teamName='" + teamName + '\'' +
                ", players=" + players +
                '}';
    }
}

