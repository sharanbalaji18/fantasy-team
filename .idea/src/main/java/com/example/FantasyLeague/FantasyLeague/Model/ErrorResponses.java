package com.example.FantasyLeague.Model;

public class ErrorResponses {
    private String error;
    private long StatusCode;
    private String path;

    public ErrorResponses() {
    }

    public ErrorResponses(String error, long statusCode, String path) {
        this.error = error;
        StatusCode = statusCode;
        this.path = path;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public long getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(long statusCode) {
        StatusCode = statusCode;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

