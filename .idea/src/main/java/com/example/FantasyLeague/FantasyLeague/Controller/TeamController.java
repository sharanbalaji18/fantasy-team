package com.example.FantasyLeague.Controller;

import com.example.FantasyLeague.Model.TeamsDto;
import com.example.FantasyLeague.Service.TeamsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teams")
public class TeamController {
    @Autowired
    private TeamsService teamsService;
    @ApiOperation(value = "to get all the teams",notes="to get all the teams for creating fantasy team")
    @ApiResponses(value={
            @ApiResponse(code = 200,message = "successfully retrieved"),
            @ApiResponse(code = 404,message = "teams not found please enter the teams")
    })
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<TeamsDto>> getAllTeams()  {

        return ResponseEntity.ok().body(teamsService.getAllTeams());

    }
    @ApiOperation(value = "to add a team",notes="to add  the teams for creating fantasy team")
    @ApiResponses(value={
            @ApiResponse(code = 201,message = "successfully created"),
            @ApiResponse(code = 404,message = "please enter correct url for creating a team")
    })
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<TeamsDto> addTeams(@RequestBody TeamsDto teamPlayersDto)  {

        TeamsDto teamPlayersDto1=teamsService.addTeams(teamPlayersDto);
        return ResponseEntity.accepted().body(teamPlayersDto1);



    }
    @ApiOperation(value = "to update the teams",notes="to update the team player parameters which you needed")
    @ApiResponses(value={
            @ApiResponse(code = 200,message = "successfully updated"),
            @ApiResponse(code = 404,message = "teams not found please enter the team for update")
    })
    @RequestMapping(method = RequestMethod.PUT,value = "/{id}")
    public ResponseEntity<HttpStatus> updateTeams(@RequestBody TeamsDto teamPlayersDto, @PathVariable long id){




        return ResponseEntity.ok().body(teamsService.updateTeams(teamPlayersDto, id));

    }
    @ApiOperation(value = "to delete the teams",notes="to delete the team which you don't need")
    @ApiResponses(value={
            @ApiResponse(code = 200,message = "successfully deleted"),
            @ApiResponse(code = 404,message = "teams not found please check whether team is present")
    })
    @RequestMapping(method=RequestMethod.DELETE,value = "/{id}")
    public  ResponseEntity<HttpStatus> deleteTeams(@PathVariable long id){
        teamsService.deleteTeams(id);
        return ResponseEntity.ok().body(HttpStatus.OK);
    }
    @ApiOperation(value = "to get all the teams with sorting",notes="to get all the teams with sorting based on teamname")
    @ApiResponses(value={
            @ApiResponse(code = 200,message = "successfully retrieved"),
            @ApiResponse(code = 404,message = "teams not found please enter the teams")
    })
    @RequestMapping(method =RequestMethod.GET,value = "/{field}")
    public ResponseEntity<List<TeamsDto>> getAllTeamsWithSorting(@PathVariable String field){
        return teamsService.getAllTeamsWithSorting(field);
    }





}

