package com.example.FantasyLeague.Repository;

import com.example.FantasyLeague.Entity.TeamPlayers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamPlayerRepository extends JpaRepository<TeamPlayers,Long> {
}
