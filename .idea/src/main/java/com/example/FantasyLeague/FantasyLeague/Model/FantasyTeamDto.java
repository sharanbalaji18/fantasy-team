package com.example.FantasyLeague.Model;

import com.example.FantasyLeague.Entity.TeamPlayers;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.List;
@ApiModel(description = "Dto for the fantasy team entity for creating fantasy teams")
public class FantasyTeamDto {
    @ApiModelProperty(name = "fantasy team name",example = "csk vs rcb",required = true)
    private String Fantasy_team_name;
    @ApiModelProperty(name = "teamPlayers for the fantasy team")
    private List<TeamPlayers> teamPlayers=new ArrayList<>(11);

    public FantasyTeamDto() {
    }

    public FantasyTeamDto( String fantasy_team_name, List<TeamPlayers> teamPlayers) {
        Fantasy_team_name = fantasy_team_name;
        this.teamPlayers = teamPlayers;
    }


    public String getFantasy_team_name() {
        return Fantasy_team_name;
    }

    public void setFantasy_team_name( String fantasy_team_name) {
        Fantasy_team_name = fantasy_team_name;
    }

    public List<TeamPlayers> getTeamPlayers() {
        return teamPlayers;
    }

    public void setTeamPlayers(List<TeamPlayers> teamPlayers) {
        this.teamPlayers = teamPlayers;
    }

    @Override
    public String toString() {
        return "FantasyTeamDto{" +
                "Fantasy_team_name='" + Fantasy_team_name + '\'' +
                ", teamPlayers=" + teamPlayers +
                '}';
    }
}

