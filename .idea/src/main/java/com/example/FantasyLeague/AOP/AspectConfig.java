package com.example.FantasyLeague.AOP;

import com.example.FantasyLeague.CustomException.NotFoundException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class AspectConfig {
    private Logger log= LoggerFactory.getLogger(AspectConfig.class);

    @Pointcut("execution(* com.example.FantasyLeague.Service.*.*(..))")
    public void loggingPointcut(){

    }
    @Before("loggingPointcut()")
    public void before(JoinPoint joinPoint){
        log.info("Before methods in the controller has been invoked:"+joinPoint.getSignature());
    }
    @After("loggingPointcut()")
    public void after(JoinPoint joinPoint){
        log.info("After methods in the controller has been invoked:"+joinPoint.getSignature());
    }
   /* @AfterThrowing(value = "execution(com.example.FantasyTeam.Service.*.*(..))",throwing = "e")
    public void afterThrowingException(JoinPoint joinPoint,NotFoundException e){
        log.info("After throwing exception in service"+joinPoint.getSignature());
        log.info("Message()"+e.getMessage());
        log.info("Path()"+e.getPath());
    }*/

    @Around("loggingPointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable{
        try {
            Object obj=joinPoint.proceed();
            return obj;
        }catch (NotFoundException e){
            log.error("Message()"+e.getMessage());
            log.error("Path()"+e.getPath());
            throw new NotFoundException(e.getMessage(), e.getPath());
        }

    }


}

