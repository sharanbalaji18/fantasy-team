package com.example.FantasyLeague.Controller;

import com.example.FantasyLeague.Model.FantasyTeamDto;
import com.example.FantasyLeague.Service.FantasyTeamService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Api(tags = "FantasyTeam")
@RestController
public class FantasyTeamController {
    @Autowired
    private FantasyTeamService fantasyTeamService;
    @ApiOperation(value = "to get all the fantasy team",notes="to get all the fantasy team")
    @ApiResponses(value={
            @ApiResponse(code = 200,message = "successfully retrieved"),
            @ApiResponse(code = 404,message = "fantasy teams not found please enter the fantasy team")
    })
    @RequestMapping(method = RequestMethod.GET,value = "/fantasyTeam")
    public ResponseEntity<List<FantasyTeamDto>> getAllFantasyTeams(){
        return fantasyTeamService.getAllFantasyTeams();
    }

    @ApiOperation(value = "to get the fantasy team with id",notes="to get fantasy team with id")
    @ApiResponses(value={
            @ApiResponse(code = 200,message = "successfully retrieved"),
            @ApiResponse(code = 404,message = "fantasy teams not found please enter the fantasy team")
    })
    @RequestMapping(method=RequestMethod.GET,value = "/fantasyTeam/id/{id}")
    public FantasyTeamDto getFantasyTeamWithId(@PathVariable long id){
        return fantasyTeamService.getFantasyTeamWithId(id);
    }

    @ApiOperation(value = "to add the fantasy team with constraints",notes="to add a fantasy team with constraints of first 6 of 1 st team and 1 st 5 of the second team")
    @ApiResponses(value={
            @ApiResponse(code = 201,message = "successfully created"),
            @ApiResponse(code = 404,message = "teams not found please enter the teams and check whether you entered with correct constraints")
    })
    @RequestMapping(method = RequestMethod.POST,value = "/teams/{id}/{id1}")
    public ResponseEntity<HttpStatus> addFantasyTeam(@RequestBody FantasyTeamDto fantasyTeamDto, @PathVariable long id, @PathVariable long id1){

        return ResponseEntity.ok().body(fantasyTeamService.addFantasyTeam(fantasyTeamDto,id,id1));

    }

    @ApiOperation(value = "to add the fantasy team without constraints",notes="to add a fantasy team without constraints but you have to check whether you have 11 players")
    @ApiResponses(value={
            @ApiResponse(code = 201,message = "successfully created"),
            @ApiResponse(code = 404,message = "teams not found please enter the teams ")
    })
    @RequestMapping(method = RequestMethod.POST,value = "fantasyTeam/{id}/{id1}")
    public ResponseEntity<FantasyTeamDto> addFantasyTeamWithoutConstraints(@RequestBody FantasyTeamDto fantasyTeamDto,@PathVariable long id,@PathVariable long id1){
        FantasyTeamDto fantasyTeamDto1=fantasyTeamService.addFantasyTeamWithoutConstraints(fantasyTeamDto,id,id1);
        return ResponseEntity.accepted().body(fantasyTeamDto1);
    }
    @ApiOperation(value = "to update the fantasy team",notes="to update the fantasy team but you have to check whether fantasy team you want to update is the fantasy team without constraints")
    @ApiResponses(value={
            @ApiResponse(code = 200,message = "successfully updated"),
            @ApiResponse(code = 404,message = "fantasy team not found please check into it")
    })

    @RequestMapping(method = RequestMethod.PUT,value = "/fantasyTeam/{id}/{id1}/{id2}")
    public ResponseEntity<HttpStatus> updateFantasyTeam(@RequestBody FantasyTeamDto fantasyTeamDto,@PathVariable long id,@PathVariable long id1,@PathVariable long id2){
        fantasyTeamService.updateFantasyTeam(fantasyTeamDto,id,id1,id2);
        return ResponseEntity.ok().body(HttpStatus.ACCEPTED);
    }
    @ApiOperation(value = "to delete the fantasy team",notes="to delete the fantasy team which you don't needed")
    @ApiResponses(value={
            @ApiResponse(code = 200,message = "successfully deleted"),
            @ApiResponse(code = 404,message = "teams not found please enter the correct fantasy team id for deleting it")
    })

    @RequestMapping(method = RequestMethod.DELETE,value = "fantasyTeam/{id}")
    public ResponseEntity<HttpStatus> deleteFantasyTeam(@PathVariable long id){
        fantasyTeamService.deleteFantasyTeam(id);
        return ResponseEntity.ok().body(HttpStatus.GONE);
    }
    @ApiOperation(value = "to sort all the fantasy teams",notes="to sort all the fantasy teams based on fantasy team name")
    @ApiResponses(value={
            @ApiResponse(code = 200,message = "successfully sorted"),
            @ApiResponse(code = 404,message = "teams not found please enter the fantasy team and check whetehr you have entered correct field for sorting")
    })
    @RequestMapping(method =RequestMethod.GET,value = "/fantasyTeam/{field}")
    public ResponseEntity<List<FantasyTeamDto>> getAllFantasyTeamsWithSorting(@PathVariable String field){
        return fantasyTeamService.getAllFantasyTeamsWithSorting(field);
    }


}

