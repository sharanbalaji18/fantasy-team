package com.example.FantasyLeague.Service;

import com.example.FantasyLeague.CustomException.BadRequestException;
import com.example.FantasyLeague.CustomException.NotFoundException;
import com.example.FantasyLeague.Entity.FantasyTeam;
import com.example.FantasyLeague.Entity.TeamPlayers;
import com.example.FantasyLeague.Entity.Teams;
import com.example.FantasyLeague.Model.FantasyTeamDto;
import com.example.FantasyLeague.Model.TeamsDto;
import com.example.FantasyLeague.Repository.FantasyTeamRepository;
import com.example.FantasyLeague.Repository.TeamsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FantasyTeamService {
    @Autowired
    private FantasyTeamRepository fantasyTeamRepository;
    @Autowired
    private TeamsRepository teamsRepository;
    Logger log = LoggerFactory.getLogger(FantasyTeamService.class);

    public FantasyTeamDto ConvertEntityToDtos(FantasyTeam fantasyTeam) {
        FantasyTeamDto fantasyTeamDto = new FantasyTeamDto();
        fantasyTeamDto.setFantasy_team_name(fantasyTeam.getFantasy_team_name());
        fantasyTeamDto.setTeamPlayers(fantasyTeam.getTeamPlayers());
        return fantasyTeamDto;
    }

    public FantasyTeam ConvertEntityToFantasyTeam(FantasyTeam fantasyTeam, FantasyTeamDto fantasyTeamDto, List<TeamPlayers> teams1, List<TeamPlayers> teams3) throws BadRequestException {
        fantasyTeam.setFantasy_team_name(fantasyTeamDto.getFantasy_team_name());


        List<TeamPlayers> fantasyTeams1 = new ArrayList<>(11);
        fantasyTeams1.addAll(teams1);
        fantasyTeams1.addAll(teams3);
        fantasyTeam.setTeamPlayers(fantasyTeams1);

        List<TeamPlayers> fantasyTeams = fantasyTeam.getTeamPlayers();
        List<String> strings = new ArrayList<>();
        for (TeamPlayers teamPlayers : fantasyTeams) {
            strings.add(teamPlayers.getPlayerName());
        }

        fantasyTeam.setTeamPlayers(fantasyTeamDto.getTeamPlayers());
        List<TeamPlayers> teamPlayers = fantasyTeam.getTeamPlayers();
        System.out.print(teamPlayers);
        List<String> strings1 = new ArrayList<>();
        for (TeamPlayers teamPlayers1 : teamPlayers) {
            strings1.add(teamPlayers1.getPlayerName());
        }

        List<String> teamNationality = new ArrayList<>();
        List<String> fantasyTeamNationality = new ArrayList<>();
        for (TeamPlayers teamPlayers1 : fantasyTeams) {
            teamNationality.add(teamPlayers1.getNationality());
        }
        for (TeamPlayers teamPlayers1 : teamPlayers) {
            fantasyTeamNationality.add(teamPlayers1.getNationality());
        }
        List<Integer> teamAge = new ArrayList<>();
        List<Integer> fantasyTeamAge = new ArrayList<>();
        for (TeamPlayers teamPlayers1 : fantasyTeams) {
            teamAge.add(teamPlayers1.getAge());
        }
        for (TeamPlayers teamPlayers1 : teamPlayers) {
            fantasyTeamAge.add(teamPlayers1.getAge());
        }
        System.out.print(strings);
        System.out.print(strings1);

        int i = fantasyTeams.size();
        if (i != 11) {
            throw new BadRequestException("check whether the fantasy team has size of 11", "/teams/{id}/{id1}");
        }
        boolean b = strings.equals(strings1);
        System.out.print(b);
        boolean b1 = teamNationality.equals(fantasyTeamNationality);
        boolean b2 = teamAge.equals(fantasyTeamAge);
        System.out.print(b1);
        System.out.print(b2);

        if (!b) {
            throw new BadRequestException("Please check whether the team with correct contraint of 6,5", "/teams/{id}/{id}");
        }
        if (!b1){
            throw new BadRequestException("Please check whether the age and nationality are correct", "/teams/{id}/{id}");
        }
        if (!b2){
            throw new BadRequestException("Please check whether the age and nationality are correct", "/teams/{id}/{id}");
        }

        return fantasyTeam;
    }

    public TeamsDto convertEntityToDto(Teams teams) {
        TeamsDto teamPlayersDto = new TeamsDto();
        teamPlayersDto.setTeamName(teams.getTeamName());
        teamPlayersDto.setPlayers(teams.getPlayers());
        return teamPlayersDto;
    }

    public FantasyTeam convertDtoToFantasyTeamWithoutConstraint(FantasyTeam fantasyTeam, FantasyTeamDto fantasyTeamDto, List<TeamPlayers> teams1, List<TeamPlayers> teams3) {
        fantasyTeam.setFantasy_team_name(fantasyTeamDto.getFantasy_team_name());


        List<String> firstTeamPlayers=new ArrayList<>();
        for(TeamPlayers teamPlayers:teams1){
            firstTeamPlayers.add(teamPlayers.getPlayerName());
        }
        List<String> secondTeamPlayers=new ArrayList<>();
        for(TeamPlayers teamPlayers:teams3){
            secondTeamPlayers.add(teamPlayers.getPlayerName());
        }

        fantasyTeam.setTeamPlayers(fantasyTeamDto.getTeamPlayers());
        List<TeamPlayers> teamPlayers = fantasyTeam.getTeamPlayers();
        List<String> strings1 = new ArrayList<>();
        for (TeamPlayers teamPlayers1 : teamPlayers) {
            strings1.add(teamPlayers1.getPlayerName());
        }
        int i = teamPlayers.size();
        if (i != 11) {
            throw new BadRequestException("check whether the fantasy team has size of 11", "/fantasyTeam/{id}/{id1}");
        }
        /*boolean b = strings.containsAll(strings1);
        if (!b) {
            throw new BadRequestException("check whether the fantasy team has the players of the team", "/fantasyTeam{id}/{id1}");
        }*/
        System.out.print(firstTeamPlayers);
        System.out.print(secondTeamPlayers);
        System.out.print(strings1);

        int countFirstTeamPlayers=0;
        for (int i1=0;i1<11;i1++){
            if(firstTeamPlayers.contains(strings1.get(i1))){
               countFirstTeamPlayers++;

            }
        }
        int countSecondTeamPlayers=0;


        for (int i1=0;i1<11;i1++){
            if(secondTeamPlayers.contains(strings1.get(i1))){
                countSecondTeamPlayers++;

            }
        }


        System.out.print(countFirstTeamPlayers);
        System.out.print(countSecondTeamPlayers);
        if (countFirstTeamPlayers!=6 ) {
            throw new BadRequestException("check whether the fantasy team has the players of the team", "/fantasyTeam{id}/{id1}");
        }
        if (countSecondTeamPlayers!=5) {
            throw new BadRequestException("check whether the fantasy team has the players of the team", "/fantasyTeam{id}/{id1}");
        }
        return fantasyTeam;


    }


    public ResponseEntity<List<FantasyTeamDto>> getAllFantasyTeams() throws NotFoundException {
        List<FantasyTeamDto> fantasyTeamDtos = fantasyTeamRepository.findAll().stream().map(fantasyTeam -> ConvertEntityToDtos(fantasyTeam)).collect(Collectors.toList());
        if (fantasyTeamDtos.isEmpty()) {
            log.error("please enter the fantasy team");
            throw new NotFoundException("fantasy teams not found", "/fantasyTeam");
        }
        log.info("fantasy teams has been got successfully");
        return new ResponseEntity<>(fantasyTeamDtos, HttpStatus.OK);
    }

    public HttpStatus addFantasyTeam(FantasyTeamDto fantasyTeamDto, long id, long id1) { //with 6,5 constraints
        FantasyTeam fantasyTeam = new FantasyTeam();
        Teams teams = teamsRepository.findById(id).orElseThrow(() -> new NotFoundException("teams not found", "teams/{id}/{id1}"));
        TeamsDto teamsDto = convertEntityToDto(teams);
        List<TeamPlayers> teams1 = teamsDto.getPlayers();
        List<TeamPlayers> teamPlayers = teams1.stream().limit(6).collect(Collectors.toList());
        System.out.print(teamPlayers);
        Teams teams2 = teamsRepository.findById(id1).orElseThrow(() -> new NotFoundException("teams not found", "teams/{id}/{id1}"));
        TeamsDto teamsDto1 = convertEntityToDto(teams2);
        List<TeamPlayers> teams3 = teamsDto1.getPlayers();
        List<TeamPlayers> teamPlayers1 = teams3.stream().limit(5).collect(Collectors.toList());
        System.out.print(teamPlayers1);

        log.info("fantasy team is going to create");
        fantasyTeamRepository.save(ConvertEntityToFantasyTeam(fantasyTeam, fantasyTeamDto, teamPlayers, teamPlayers1));
        log.info("fantasy team created successfully");
        return HttpStatus.CREATED;
    }


    public FantasyTeamDto addFantasyTeamWithoutConstraints(FantasyTeamDto fantasyTeamDto, long id, long id1) {
        FantasyTeam fantasyTeam = new FantasyTeam();
        Teams teams = teamsRepository.findById(id).orElseThrow(() -> new NotFoundException("teams not found", "fantasyTeam/{id}/{id1}"));
        TeamsDto teamsDto = convertEntityToDto(teams);
        List<TeamPlayers> teams1 = teamsDto.getPlayers();
        List<TeamPlayers> teamPlayers = teams1.stream().limit(15).collect(Collectors.toList());

        Teams teams2 = teamsRepository.findById(id1).orElseThrow(() -> new NotFoundException("teams not found", "fantasyTeam/{id}/{id1}"));
        TeamsDto teamsDto1 = convertEntityToDto(teams2);
        List<TeamPlayers> teams3 = teamsDto1.getPlayers();
        List<TeamPlayers> teamPlayers1 = teams3.stream().limit(15).collect(Collectors.toList());


        log.info("fantasy team is going to create");
        fantasyTeamRepository.save(convertDtoToFantasyTeamWithoutConstraint(fantasyTeam, fantasyTeamDto, teamPlayers, teamPlayers1));
        log.info("fantasy team created successfully");
        return fantasyTeamDto;
    }

    public HttpStatus updateFantasyTeam(FantasyTeamDto fantasyTeamDto, long id, long id1, long id2) {
        FantasyTeam fantasyTeamUpdate = fantasyTeamRepository.findById(id2).orElseThrow(() -> new NotFoundException("teams you want to update is not found", "/fantasyTeam/{id}"));
        Teams teams = teamsRepository.findById(id).orElseThrow(() -> new NotFoundException("teams not found", "fantasyTeam/{id}/{id1}/{id2}"));
        List<TeamPlayers> teams1 = teams.getPlayers();
        teams1.stream().limit(15).collect(Collectors.toList());
        Teams teams2 = teamsRepository.findById(id1).orElseThrow(() -> new NotFoundException("teams not found", "fantasyTeam/{id}/{id1}/{id2}"));
        List<TeamPlayers> teams3 = teams2.getPlayers();
        teams3.stream().limit(15).collect(Collectors.toList());
        log.info("fantasy team going to update");
        fantasyTeamRepository.save(convertDtoToFantasyTeamWithoutConstraint(fantasyTeamUpdate, fantasyTeamDto, teams1, teams3));
        log.info("fantasy team updated successfully");
        return HttpStatus.OK;
    }

    public ResponseEntity<HttpStatus> deleteFantasyTeam(long id) {
        FantasyTeam fantasyTeamDelete = fantasyTeamRepository.findById(id).orElseThrow(() -> new NotFoundException("fantasyTeam you want to delete is not found", "/fantasy/{id}"));
        fantasyTeamRepository.delete(fantasyTeamDelete);
        log.info("fantasy team {id} is deleted successfully");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<List<FantasyTeamDto>> getAllFantasyTeamsWithSorting(String field) throws NotFoundException {
        List<FantasyTeamDto> fantasyTeamDtos = fantasyTeamRepository.findAll(Sort.by(Sort.Direction.ASC, field)).stream().map(fantasyTeam -> ConvertEntityToDtos(fantasyTeam)).collect(Collectors.toList());
        if (fantasyTeamDtos.isEmpty()) {
            log.error("please enter the fantasy team");
            throw new NotFoundException("fantasy teams not found", "/fantasyTeam");
        }
        log.info("fantasy teams has been got successfully");
        return new ResponseEntity<>(fantasyTeamDtos, HttpStatus.OK);

    }
    public FantasyTeamDto getFantasyTeamWithId(long id) {
        FantasyTeam fantasyTeam=fantasyTeamRepository.findById(id).orElseThrow(()->new NotFoundException("fantasy teams not found", "/fantasyTeam"));
        FantasyTeamDto fantasyTeamDto=ConvertEntityToDtos(fantasyTeam);
        return fantasyTeamDto;
    }



}
