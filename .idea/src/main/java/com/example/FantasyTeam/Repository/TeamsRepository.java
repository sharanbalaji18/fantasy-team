package com.example.FantasyTeam.Repository;

import com.example.FantasyTeam.Entity.Teams;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamsRepository extends JpaRepository<Teams,Long> {

}
