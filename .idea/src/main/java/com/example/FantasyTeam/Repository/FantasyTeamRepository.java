package com.example.FantasyTeam.Repository;

import com.example.FantasyTeam.Entity.FantasyTeam;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FantasyTeamRepository extends JpaRepository <FantasyTeam,Long>{

}
