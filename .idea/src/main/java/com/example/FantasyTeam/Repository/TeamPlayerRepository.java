package com.example.FantasyTeam.Repository;

import com.example.FantasyTeam.Entity.TeamPlayers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamPlayerRepository extends JpaRepository<TeamPlayers,Long> {

}
