package com.example.FantasyTeam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FantasyTeamApplication {

	public static void main(String[] args) {
		SpringApplication.run(FantasyTeamApplication.class, args);
	}

}
