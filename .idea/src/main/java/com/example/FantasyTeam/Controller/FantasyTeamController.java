package com.example.FantasyTeam.Controller;

import com.example.FantasyTeam.Model.FantasyTeamDto;
import com.example.FantasyTeam.Service.FantasyTeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FantasyTeamController {
    @Autowired
    private FantasyTeamService fantasyTeamService;
    @RequestMapping(method = RequestMethod.GET,value = "/fantasyTeam")
    public ResponseEntity<List<FantasyTeamDto>> getAllFantasyTeams(){
        return fantasyTeamService.getAllFantasyTeams();
    }
    @RequestMapping(method = RequestMethod.POST,value = "/teams/{id}/{id1}")
    public ResponseEntity<HttpStatus> addFantasyTeam(@RequestBody FantasyTeamDto fantasyTeamDto,@PathVariable long id,@PathVariable long id1){
        fantasyTeamService.addFantasyTeam(fantasyTeamDto,id,id1);
        return ResponseEntity.accepted().body(HttpStatus.CREATED);

    }
    @RequestMapping(method = RequestMethod.PUT,value = "/teams/{id}/{id1}/{id2}")
    public ResponseEntity<HttpStatus> updateFantasyTeam(@RequestBody FantasyTeamDto fantasyTeamDto,@PathVariable long id,@PathVariable long id1,@PathVariable long id2){
        fantasyTeamService.updateFantasyTeam(fantasyTeamDto,id,id1,id2);
        return ResponseEntity.ok().body(HttpStatus.ACCEPTED);
    }
}
