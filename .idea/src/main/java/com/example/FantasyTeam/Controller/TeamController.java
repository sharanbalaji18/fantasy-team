package com.example.FantasyTeam.Controller;

import com.example.FantasyTeam.Model.TeamPlayersDto;
import com.example.FantasyTeam.Service.TeamsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TeamController {
@Autowired
    private TeamsService teamsService;
@RequestMapping(method = RequestMethod.GET,value = "/teams")
    public ResponseEntity<List<TeamPlayersDto>> getAllTeams()  {

    return teamsService.getAllTeams();

}

@RequestMapping(method = RequestMethod.POST,value = "/teams")
    public ResponseEntity<TeamPlayersDto> addTeams(@RequestBody TeamPlayersDto teamPlayersDto)  {

        TeamPlayersDto teamPlayersDto1=teamsService.addTeams(teamPlayersDto);
        return ResponseEntity.accepted().body(teamPlayersDto1);



    }

@RequestMapping(method = RequestMethod.PUT,value = "/teams/{id}")
    public ResponseEntity<HttpStatus> updateTeams(@RequestBody TeamPlayersDto teamPlayersDto,@PathVariable long id){



        teamsService.updateTeams(teamPlayersDto, id);
        return ResponseEntity.ok().body(HttpStatus.ACCEPTED);

    }
@RequestMapping(method=RequestMethod.DELETE,value = "/teams/{id}")
      public  ResponseEntity<HttpStatus> deleteTeams(@PathVariable long id){
     teamsService.deleteTeams(id);
     return ResponseEntity.ok().body(HttpStatus.OK);
}



}
