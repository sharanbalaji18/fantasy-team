package com.example.FantasyTeam.Service;

import com.example.FantasyTeam.CustomExceptions.NotFoundException;
import com.example.FantasyTeam.Entity.FantasyTeam;
import com.example.FantasyTeam.Entity.TeamPlayers;
import com.example.FantasyTeam.Entity.Teams;
import com.example.FantasyTeam.Model.FantasyTeamDto;
import com.example.FantasyTeam.Repository.FantasyTeamRepository;
import com.example.FantasyTeam.Repository.TeamsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FantasyTeamService {
    @Autowired
    private FantasyTeamRepository fantasyTeamRepository;
    @Autowired
    private TeamsRepository teamsRepository;
    Logger log= LoggerFactory.getLogger(FantasyTeamService.class);

    private FantasyTeamDto ConvertEntityToDto(FantasyTeam fantasyTeam){
        FantasyTeamDto fantasyTeamDto=new FantasyTeamDto();
        fantasyTeamDto.setFantasy_team_name(fantasyTeam.getFantasy_team_name());
        fantasyTeamDto.setTeamPlayers(fantasyTeam.getTeamPlayers());
        return fantasyTeamDto;
    }
    private FantasyTeam ConvertEntityToFantasyTeam(FantasyTeam fantasyTeam,FantasyTeamDto fantasyTeamDto,List<TeamPlayers> teams1,List<TeamPlayers> teams3){
        fantasyTeam.setFantasy_team_name(fantasyTeamDto.getFantasy_team_name());
        fantasyTeam.setTeamPlayers(fantasyTeamDto.getTeamPlayers());
        List<TeamPlayers> fantasyTeams=new ArrayList<>(11);
        fantasyTeams=fantasyTeam.getTeamPlayers();
        List<TeamPlayers> fantasyTeams1=new ArrayList<>(11);
        fantasyTeams1.addAll(teams1);
        fantasyTeams1.addAll(teams3);
        int i=fantasyTeams.size();
        if(i!=11 && fantasyTeams!=fantasyTeams1){
            throw new RuntimeException();
        }
        return fantasyTeam;
    }

    public ResponseEntity<List<FantasyTeamDto>> getAllFantasyTeams() throws NotFoundException {
        List<FantasyTeamDto> fantasyTeamDtos=fantasyTeamRepository.findAll().stream().map(fantasyTeam -> ConvertEntityToDto(fantasyTeam)).collect(Collectors.toList());
        if(fantasyTeamDtos.isEmpty()){
            log.error("please enter the fantasy team");
            throw new NotFoundException("fantasy teams not found","/fantasyTeam");
        }
        return new ResponseEntity<>(fantasyTeamDtos, HttpStatus.OK);
    }

    public void addFantasyTeam(FantasyTeamDto fantasyTeamDto,long id,long id1) {
        FantasyTeam fantasyTeam=new FantasyTeam();
        Teams teams=teamsRepository.findById(id).orElseThrow();
        List<TeamPlayers> teams1=teams.getPlayers();
        teams1.stream().limit(6).collect(Collectors.toList());
        Teams teams2=teamsRepository.findById(id1).orElseThrow();
        List<TeamPlayers> teams3=teams.getPlayers();
        teams3.stream().limit(5).collect(Collectors.toList());

        log.info("fantasy team is going to create");
        fantasyTeamRepository.save(ConvertEntityToFantasyTeam(fantasyTeam,fantasyTeamDto,teams1,teams3));
        log.info("fantasy team created successfully");
    }


    public void updateFantasyTeam(FantasyTeamDto fantasyTeamDto, long id,long id1,long id2) {
        FantasyTeam fantasyTeamUpdate=fantasyTeamRepository.findById(id2).orElseThrow(() ->new NotFoundException("teams you want to update is not found","/fantasyTeam/{id}"));
        Teams teams=teamsRepository.findById(id).orElseThrow();
        List<TeamPlayers> teams1=teams.getPlayers();
        teams1.stream().limit(6).collect(Collectors.toList());
        Teams teams2=teamsRepository.findById(id1).orElseThrow();
        List<TeamPlayers> teams3=teams.getPlayers();
        teams3.stream().limit(5).collect(Collectors.toList());
        log.info("fantasy team going to update");
        fantasyTeamRepository.save(ConvertEntityToFantasyTeam(fantasyTeamUpdate,fantasyTeamDto,teams1,teams3));
        log.info("fantasy team updated successfully");
    }
}
