package com.example.FantasyTeam.Service;

import com.example.FantasyTeam.CustomExceptions.NotFoundException;
import com.example.FantasyTeam.Entity.TeamPlayers;
import com.example.FantasyTeam.Entity.Teams;
import com.example.FantasyTeam.Model.TeamPlayersDto;
import com.example.FantasyTeam.Repository.TeamPlayerRepository;
import com.example.FantasyTeam.Repository.TeamsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeamsService {
    @Autowired
    private TeamsRepository teamsRepository;
    @Autowired
    private TeamPlayerRepository teamPlayerRepository;

    Logger log= LoggerFactory.getLogger(TeamsService.class);

    public ResponseEntity<List<TeamPlayersDto>> getAllTeams() throws NotFoundException{
        List<TeamPlayersDto>teamPlayersDtoList=teamsRepository.findAll().stream().map(teams -> convertEntityToDto(teams)).collect(Collectors.toList());
       log.debug("Checking whether the list is empty");
        if(teamPlayersDtoList.isEmpty()){
            log.error("Exception in getAllTeams method");
            throw new NotFoundException("teams not found ","/teams");
        }
        log.info("returning the list");
        return new ResponseEntity<>(teamPlayersDtoList, HttpStatus.OK);
    }
    private TeamPlayersDto convertEntityToDto(Teams teams){
        TeamPlayersDto teamPlayersDto=new TeamPlayersDto();
        teamPlayersDto.setTeamName(teams.getTeamName());
        teamPlayersDto.setPlayers(teams.getPlayers());


       /* teamPlayersDto.setPlayer1(teamPlayers.getPlayer1());
        teamPlayersDto.setPlayer2(teamPlayers.getPlayer2());
        teamPlayersDto.setPlayer3(teamPlayers.getPlayer3());
        teamPlayersDto.setPlayer4(teamPlayers.getPlayer4());
        teamPlayersDto.setPlayer5(teamPlayers.getPlayer5());
        teamPlayersDto.setPlayer6(teamPlayers.getPlayer6());
        teamPlayersDto.setPlayer7(teamPlayers.getPlayer7());
        teamPlayersDto.setPlayer8(teamPlayers.getPlayer8());
        teamPlayersDto.setPlayer9(teamPlayers.getPlayer9());
        teamPlayersDto.setPlayer10(teamPlayers.getPlayer10());
        teamPlayersDto.setPlayer11(teamPlayers.getPlayer11());
        teamPlayersDto.setPlayer12(teamPlayers.getPlayer12());
        teamPlayersDto.setPlayer13(teamPlayers.getPlayer13());
        teamPlayersDto.setPlayer14(teamPlayers.getPlayer14());
        teamPlayersDto.setPlayer15(teamPlayers.getPlayer15());*/
        return teamPlayersDto;
    }
    private Teams convertEntityToTeams(Teams teams,TeamPlayersDto teamPlayersDto){
        teams.setTeamName(teamPlayersDto.getTeamName());
        teams.setPlayers(teamPlayersDto.getPlayers());
        return teams;

    }



    public TeamPlayersDto addTeams(TeamPlayersDto teamPlayersDto) {
        Teams teams=new Teams();
         teamsRepository.save(convertEntityToTeams(teams,teamPlayersDto));
         return teamPlayersDto;

    }

    public ResponseEntity<HttpStatus> updateTeams(TeamPlayersDto teamPlayersDto, long id) {
        Teams teamUpdate=teamsRepository.findById(id).orElseThrow(()->new RuntimeException());
        teamsRepository.save(convertEntityToTeams(teamUpdate,teamPlayersDto));

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    public ResponseEntity<HttpStatus> deleteTeams(long id) {
        Teams teamDelete=teamsRepository.findById(id).orElseThrow(()->new RuntimeException());

        TeamPlayersDto teamPlayersDto=new TeamPlayersDto();
        teamsRepository.save(convertEntityToTeams(teamDelete,teamPlayersDto));

        return new ResponseEntity<>(HttpStatus.OK);

    }

}
