package com.example.FantasyTeam.Advice;

import com.example.FantasyTeam.CustomExceptions.BadRequestException;
import com.example.FantasyTeam.CustomExceptions.NotFoundException;
import com.example.FantasyTeam.Model.ErrorResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@org.springframework.web.bind.annotation.ControllerAdvice
public class ControllerAdvice extends ResponseEntityExceptionHandler {
    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    ErrorResponses onNotFoundException(NotFoundException e){
        ErrorResponses errorResponses=new ErrorResponses();
        errorResponses.setError(e.getMessage());
        errorResponses.setStatusCode(404);
        errorResponses.setPath(e.getPath());
        return errorResponses;
    }



    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ErrorResponses onBadRequestException(BadRequestException e){
        ErrorResponses errorResponses=new ErrorResponses();
        errorResponses.setError(e.getMessage());
        errorResponses.setStatusCode(400);
        errorResponses.setPath(e.getPath());
        return errorResponses;
    }



}
