package com.example.FantasyTeam.Model;

import com.example.FantasyTeam.Entity.TeamPlayers;
import com.example.FantasyTeam.Entity.Teams;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class FantasyTeamDto {
    private String Fantasy_team_name;
    private List<TeamPlayers> teamPlayers=new ArrayList<>(11);
}
