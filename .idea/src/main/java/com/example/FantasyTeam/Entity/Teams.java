package com.example.FantasyTeam.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@ToString
@Entity
@Table(name = "Teams")
public class Teams {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "team_name",nullable = false,unique = true)
    private String teamName;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="team_id",referencedColumnName = "id")

    List<TeamPlayers> players=new ArrayList<>(15);


    public Teams() {
    }

    public Teams(Long id, String teamName, List<TeamPlayers> players) {
        this.id = id;
        this.teamName = teamName;
        this.players = players;


    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public List<TeamPlayers> getPlayers() {
        return players;
    }

    public void setPlayers(List<TeamPlayers> players) {
        this.players= players;
    }


}
