package com.example.FantasyTeam.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@ToString
@Table(name = "Players")
public class TeamPlayers {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    private Long id;

    private String playerName;

    private int Age;
    private Role role;
    private String Nationality;
    @ManyToOne(cascade = CascadeType.ALL)
    private Teams teams;
    @ManyToOne(cascade = CascadeType.ALL)
    private FantasyTeam fantasyTeam;


    public enum Role{
        BATSMAN,
        WICKETKEEPER,
        ALLROUNDER,
        BOWLER,
    }







}
