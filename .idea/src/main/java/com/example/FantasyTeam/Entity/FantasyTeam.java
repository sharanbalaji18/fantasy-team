package com.example.FantasyTeam.Entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.ArrayList;

@Entity
@Table
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class FantasyTeam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Fantasy_Team_Id;

    private String Fantasy_team_name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "fantasy_team_id")

     List<TeamPlayers> teamPlayers=new ArrayList<>(11);



}
