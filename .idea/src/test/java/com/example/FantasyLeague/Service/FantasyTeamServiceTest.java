package com.example.FantasyLeague.Service;

import com.example.FantasyLeague.Entity.FantasyTeam;
import com.example.FantasyLeague.Entity.TeamPlayers;
import com.example.FantasyLeague.Entity.Teams;
import com.example.FantasyLeague.Model.FantasyTeamDto;
import com.example.FantasyLeague.Model.TeamsDto;
import com.example.FantasyLeague.Repository.FantasyTeamRepository;
import com.example.FantasyLeague.Repository.TeamsRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
public class FantasyTeamServiceTest {
    @InjectMocks
    private FantasyTeamService fantasyTeamService;
    @Mock
    private FantasyTeamRepository fantasyTeamRepository;
    @Mock
    private TeamsRepository teamsRepository;

    @Test
    public void addTeamsTest(){
        Teams teams =new Teams(1L,"csk", Arrays.asList(new TeamPlayers(1L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(2L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(3L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(4L,"rj",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(5L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(6L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(7L,"ru",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(8L,"ambati",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(9L,"hussey",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(10L,"mc",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(11L,"preto",31, TeamPlayers.Role.BOWLER,"sa",null,null)));
        Teams teams1=new Teams(2L,"rcb",Arrays.asList(new TeamPlayers(12L,"rainas",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(13L,"devons",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(14L,"rgs",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(15L,"rjs",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(16L,"djs",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(17L,"msds",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(18L,"rus",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(19L,"ambatis",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(20L,"husseys",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(21L,"mcs",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(22L,"pretos",31, TeamPlayers.Role.BOWLER,"sa",null,null)));
        when(teamsRepository.findById(1L)).thenReturn(Optional.of(teams));
        when(teamsRepository.findById(2L)).thenReturn(Optional.of(teams1));
        TeamsDto teamsDto = fantasyTeamService.convertEntityToDto(teams);
        List<TeamPlayers> teams2 = teamsDto.getPlayers();
        List<TeamPlayers> teamPlayers = teams2.stream().limit(6).collect(Collectors.toList());
        System.out.print(teamPlayers);
        TeamsDto teamsDto1 = fantasyTeamService.convertEntityToDto(teams1);
        List<TeamPlayers> teams3 = teamsDto1.getPlayers();
        List<TeamPlayers> teamPlayers1 = teams3.stream().limit(5).collect(Collectors.toList());
        System.out.print(teamPlayers1);
        //FantasyTeam fantasyTeam=new FantasyTeam(1L,"csk vs rcb",Arrays.asList(new TeamPlayers(1L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(2L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(3L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(4L,"rj",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(5L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(6L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(12L,"rainas",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(13L,"devons",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(14L,"rgs",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(15L,"rjs",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(16L,"djs",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null)));
        FantasyTeamDto fantasyTeamDto=new FantasyTeamDto("csk vs rcb",Arrays.asList(new TeamPlayers(1L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(2L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(3L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(4L,"rj",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(5L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(6L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(12L,"rainas",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(13L,"devons",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(14L,"rgs",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(15L,"rjs",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(16L,"djs",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null)));
        FantasyTeam fantasyTeam=fantasyTeamService.ConvertEntityToFantasyTeam(new FantasyTeam(),fantasyTeamDto,teamPlayers,teamPlayers1);
        when(fantasyTeamRepository.save(any(FantasyTeam.class))).thenReturn(fantasyTeam);
        assertEquals(HttpStatus.CREATED,fantasyTeamService.addFantasyTeam(fantasyTeamDto,1L,2L));

    }
    @Test
    public void addTeamsWithoutContraintsTest(){
        Teams teams =new Teams(1L,"csk", Arrays.asList(new TeamPlayers(1L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(2L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(3L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(4L,"rj",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(5L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(6L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(7L,"ru",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(8L,"ambati",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(9L,"hussey",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(10L,"mc",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(11L,"preto",31, TeamPlayers.Role.BOWLER,"sa",null,null)));
        Teams teams1=new Teams(2L,"rcb",Arrays.asList(new TeamPlayers(12L,"rainas",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(13L,"devons",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(14L,"rgs",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(15L,"rjs",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(16L,"djs",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(17L,"msds",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(18L,"rus",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(19L,"ambatis",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(20L,"husseys",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(21L,"mcs",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(22L,"pretos",31, TeamPlayers.Role.BOWLER,"sa",null,null)));
        when(teamsRepository.findById(1L)).thenReturn(Optional.of(teams));
        when(teamsRepository.findById(2L)).thenReturn(Optional.of(teams1));
        TeamsDto teamsDto = fantasyTeamService.convertEntityToDto(teams);
        List<TeamPlayers> teams2 = teamsDto.getPlayers();
        List<TeamPlayers> teamPlayers = teams2.stream().limit(15).collect(Collectors.toList());
        System.out.print(teamPlayers);
        TeamsDto teamsDto1 = fantasyTeamService.convertEntityToDto(teams1);
        List<TeamPlayers> teams3 = teamsDto1.getPlayers();
        List<TeamPlayers> teamPlayers1 = teams3.stream().limit(15).collect(Collectors.toList());
        System.out.print(teamPlayers1);
        FantasyTeamDto fantasyTeamDto=new FantasyTeamDto("csk vs rcb",Arrays.asList(new TeamPlayers(1L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(2L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(14L,"rgs",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(3L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(5L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(6L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(7L,"ru",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(19L,"ambatis",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(20L,"husseys",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(21L,"mcs",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(22L,"pretos",31, TeamPlayers.Role.BOWLER,"sa",null,null)));
        FantasyTeam fantasyTeam=fantasyTeamService.convertDtoToFantasyTeamWithoutConstraint(new FantasyTeam(),fantasyTeamDto,teamPlayers,teamPlayers1);
        when(fantasyTeamRepository.save(any(FantasyTeam.class))).thenReturn(fantasyTeam);
        assertEquals(fantasyTeam.getFantasy_team_name(),fantasyTeamService.addFantasyTeamWithoutConstraints(fantasyTeamDto,1L,2L).getFantasy_team_name());

    }
    @Test
    public void UpdateTeamsTest(){
        Teams teams =new Teams(1L,"csk", Arrays.asList(new TeamPlayers(1L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(2L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(3L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(4L,"rj",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(5L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(6L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(7L,"ru",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(8L,"ambati",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(9L,"hussey",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(10L,"mc",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(11L,"preto",31, TeamPlayers.Role.BOWLER,"sa",null,null)));
        Teams teams1=new Teams(2L,"rcb",Arrays.asList(new TeamPlayers(12L,"rainas",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(13L,"devons",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(14L,"rgs",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(15L,"rjs",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(16L,"djs",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(17L,"msds",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(18L,"rus",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(19L,"ambatis",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(20L,"husseys",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(21L,"mcs",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(22L,"pretos",31, TeamPlayers.Role.BOWLER,"sa",null,null)));
        when(teamsRepository.findById(1L)).thenReturn(Optional.of(teams));
        when(teamsRepository.findById(2L)).thenReturn(Optional.of(teams1));
        TeamsDto teamsDto = fantasyTeamService.convertEntityToDto(teams);
        List<TeamPlayers> teams2 = teamsDto.getPlayers();
        List<TeamPlayers> teamPlayers = teams2.stream().limit(15).collect(Collectors.toList());
        System.out.print(teamPlayers);
        TeamsDto teamsDto1 = fantasyTeamService.convertEntityToDto(teams1);
        List<TeamPlayers> teams3 = teamsDto1.getPlayers();
        List<TeamPlayers> teamPlayers1 = teams3.stream().limit(15).collect(Collectors.toList());
        System.out.print(teamPlayers1);
        FantasyTeamDto fantasyTeamDto=new FantasyTeamDto("csk vs rcb",Arrays.asList(new TeamPlayers(1L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(2L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(14L,"rgs",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(3L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(5L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(6L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(7L,"ru",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(19L,"ambatis",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(20L,"husseys",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(21L,"mcs",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(22L,"pretos",31, TeamPlayers.Role.BOWLER,"sa",null,null)));
        FantasyTeam fantasyTeam=new FantasyTeam(1L,"csk vs rcb",Arrays.asList(new TeamPlayers(1L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(2L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(14L,"rgs",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(3L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(5L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(6L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(7L,"ru",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(19L,"ambatis",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(20L,"husseys",42, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(21L,"mcs",24, TeamPlayers.Role.BOWLER,"india",null,null),new TeamPlayers(22L,"pretos",31, TeamPlayers.Role.BOWLER,"sa",null,null)));
        FantasyTeam fantasyTeamUpdate=new FantasyTeam(1L,"rcb vs csk",Arrays.asList(new TeamPlayers(1L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(2L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(14L,"rgs",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(3L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(5L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(6L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(7L,"ru",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(19L,"ambatis",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(20L,"husseys",42, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(21L,"mcs",24, TeamPlayers.Role.BOWLER,"india",null,null),new TeamPlayers(22L,"pretos",31, TeamPlayers.Role.BOWLER,"sa",null,null)));
        when(fantasyTeamRepository.findById(1L)).thenReturn(Optional.of(fantasyTeam));
        FantasyTeam fantasyTeam1=fantasyTeamService.convertDtoToFantasyTeamWithoutConstraint(fantasyTeamUpdate,fantasyTeamDto,teamPlayers,teamPlayers1);
        when(fantasyTeamRepository.save(any(FantasyTeam.class))).thenReturn(fantasyTeam1);
        assertEquals(HttpStatus.OK,fantasyTeamService.updateFantasyTeam(fantasyTeamDto,1L,2L,1L));
    }
}
