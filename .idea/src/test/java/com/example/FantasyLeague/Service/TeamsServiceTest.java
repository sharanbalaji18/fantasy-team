package com.example.FantasyLeague.Service;

import com.example.FantasyLeague.Entity.TeamPlayers;
import com.example.FantasyLeague.Entity.Teams;
import com.example.FantasyLeague.Model.TeamsDto;
import com.example.FantasyLeague.Repository.TeamsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import javax.persistence.Id;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
public class TeamsServiceTest {
    @InjectMocks
    private  TeamsService teamsService;
    @Mock
     private TeamsRepository teamsRepository;

private TeamsDto teamsDto;
private TeamsDto teamsDto1;

    @BeforeEach
    public void init(){
       teamsDto=new TeamsDto("csk",Arrays.asList(new TeamPlayers(1L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(2L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(3L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(4L,"rj",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(5L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(6L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(7L,"ru",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(8L,"ambati",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(9L,"hussey",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(10L,"mc",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(11L,"preto",31, TeamPlayers.Role.BOWLER,"sa",null,null)));
        teamsDto1=new TeamsDto("rcb",Arrays.asList(new TeamPlayers(12L,"rainas",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(13L,"devons",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(14L,"rgs",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(15L,"rjs",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(16L,"djs",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(17L,"msds",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(18L,"rus",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(19L,"ambatis",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(20L,"husseys",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(21L,"mcs",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(22L,"pretos",31, TeamPlayers.Role.BOWLER,"sa",null,null)));


    }
    @Test
    public void addTeamsTest(){
        Teams teams=teamsService.convertEntityToTeams(new Teams(),teamsDto);
        when(teamsRepository.save(any(Teams.class))).thenReturn(teams);
        assertEquals(teams.getTeamName(),teamsService.addTeams(teamsDto).getTeamName());


}
    @Test
    public void getAllTeamsTest(){
       /* List<Teams> teamsList=new ArrayList<>();
        teamsList.add(new Teams(1L,"csk",Arrays.asList(new TeamPlayers(1L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(2L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(3L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(4L,"rj",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(5L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(6L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(7L,"ru",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(8L,"ambati",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(9L,"hussey",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(10L,"mc",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(11L,"preto",31, TeamPlayers.Role.BOWLER,"sa",null,null))));
        teamsList.add(new Teams(2L,"rcb",Arrays.asList(new TeamPlayers(12L,"rainas",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(13L,"devons",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(14L,"rgs",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(15L,"rjs",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(16L,"djs",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(17L,"msds",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(18L,"rus",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(19L,"ambatis",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(20L,"husseys",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(21L,"mcs",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(22L,"pretos",31, TeamPlayers.Role.BOWLER,"sa",null,null))));*/
        Teams teams=teamsService.convertEntityToTeams(new Teams(),teamsDto);
        when(teamsRepository.save(any(Teams.class))).thenReturn(teams);

        Teams teams1=teamsService.convertEntityToTeams(new Teams(),teamsDto1);
        when(teamsRepository.save(any(Teams.class))).thenReturn(teams1);

        List<TeamsDto> teamsDtoList=new ArrayList<>();

        when(teamsDtoList=teamsRepository.findAll().stream().map(teams2 -> teamsService.convertEntityToDto(teams)).collect(Collectors.toList())).thenReturn(teamsDtoList);

        assertEquals(1,teamsService.getAllTeams());


    }
    @Test
    public void updateTeamsTest(){
        Teams teams =new Teams(1L,"csk",Arrays.asList(new TeamPlayers(1L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(2L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(3L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(4L,"rj",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(5L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(6L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(7L,"ru",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(8L,"ambati",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(9L,"hussey",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(10L,"mc",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(11L,"preto",31, TeamPlayers.Role.BOWLER,"sa",null,null)));
        Teams teamsForUpdate=new Teams(1L,"mi",Arrays.asList(new TeamPlayers(12L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(13L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(14L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(15L,"rj",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(16L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(17L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(18L,"ru",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(19L,"ambati",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(20L,"hussey",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(21L,"mc",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(22L,"preto",31, TeamPlayers.Role.BOWLER,"sa",null,null)));
        when(teamsRepository.findById(any(Long.class))).thenReturn(Optional.of(teams));
        when(teamsRepository.save(any(Teams.class))).thenReturn(teamsForUpdate);
        assertEquals(HttpStatus.ACCEPTED,teamsService.updateTeams(teamsDto,1L));


    }
   /* @Test
    public void getAllTeamsTests(){
        Teams teams =new Teams(1L,"csk",Arrays.asList(new TeamPlayers(1L,"raina",22, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(2L,"devon",24, TeamPlayers.Role.ALLROUNDER,"nz",null,null),new TeamPlayers(3L,"rg",24, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(4L,"rj",24, TeamPlayers.Role.ALLROUNDER,"india",null,null),new TeamPlayers(5L,"dj",30, TeamPlayers.Role.ALLROUNDER,"wi",null,null),new TeamPlayers(6L,"msd",38, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(7L,"ru",36, TeamPlayers.Role.BATSMAN,"india",null,null),new TeamPlayers(8L,"ambati",35, TeamPlayers.Role.WICKETKEEPER,"india",null,null),new TeamPlayers(9L,"hussey",42, TeamPlayers.Role.BATSMAN,"aus",null,null),new TeamPlayers(10L,"mc",24, TeamPlayers.Role.BOWLER,"ind",null,null),new TeamPlayers(11L,"preto",31, TeamPlayers.Role.BOWLER,"sa",null,null)));

        when(teamsRepository.findAll()).thenReturn(Stream.of(teamsService.convertEntityToDto()).collect(Collectors.toList()));
        assertEquals(1,teamsService.getAllTeams().size());
    }*/


}
